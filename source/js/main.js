/*global
$, document, this, console, for
*/
/*jslint this:true */

'use strict';

var path= '',
    baseurl = '/',
    uploads = 'images/';

if (path == 'PROD') {
    baseurl = '/';
    uploads = 'wp-content/uploads/';
}

basedir = baseurl + uploads;

var $section_hero,
    $hero,
    dragging,
    $search,
    $form_search,
    $btn_search,
    $brand_details,
    $brand_carousel,
    $input_toggle,
    $input_checkbox,
    $form_subscribe,
    $form_subscribe_input,
    $form_subscribe_input_checkbox,
    $btn_subscribe,
    $btn_modal;

var cache = {
    load: function () {
        $section_hero = $('.psm-hero');
        $hero = $section_hero.find('#psm-hero__carousel');
        dragging = true;
        $search = $('.psm-search');
        $form_search = $search.find('.psm-search__form');
        $btn_search = $search.find('.psm-btn__search');
        $brand_details = $('.psm-brand__details');
        $brand_carousel = $brand_details.find('.psm-brand__carousel');
        $form_subscribe = $('.psm-subscribe__form');
        $form_subscribe_input = $form_subscribe.find('.psm-input');
        $form_subscribe_input_checkbox = $form_subscribe.find('.psm-input:checkbox');
        $input_toggle = $('.psm-input__toggle');
        $input_checkbox = $form_subscribe.find('.psm-subscribe__interes .psm-input');
        $btn_subscribe = $form_subscribe.find('.psm-btn__subscribe');
        $btn_modal = $('.psm-btn__modal');
    }
};

var psmSliderHero = {
    load: function () {
        $hero.owlCarousel({
            autoPlay: 5000,
            stopOnHover: true,
            navigation: true,
            pagination: true,
            singleItem: true,
            addClassActive: true,
            transitionStyle: 'fade',
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

            afterInit: function() {
                psmSliderHero.fadeInLeft();
                psmSliderHero.fadeInRight();
            },
		
            afterMove: function() {
                psmSliderHero.fadeInLeft();
                psmSliderHero.fadeInRight();
            },
            
            afterUpdate: function() {
                psmSliderHero.fadeInLeft();
                psmSliderHero.fadeInRight();
            },
            
            startDragging: function() {
                dragging = true;
            },
            
            afterAction: function() {
                psmSliderHero.fadeInLeftReset();
                psmSliderHero.fadeInRightReset();
                dragging = false;
            }
        });
    },

    fadeInLeft: function() {
		$hero.find('.active .psm-hero__caption .fadeInLeft-1').stop().delay(500).animate({ opacity: 1, left: '0' }, { duration: 800, easing: 'easeOutCubic' });
		$hero.find('.active .psm-hero__caption .fadeInLeft-2').stop().delay(700).animate({ opacity: 1, left: '0' }, { duration: 800, easing: 'easeOutCubic' });
		$hero.find('.active .psm-hero__caption .fadeInLeft-3').stop().delay(1000).animate({ opacity: 1, left: "0" }, { duration: 800, easing: 'easeOutCubic' });
	},
	
	fadeInRight: function() {
		$hero.find('.active .psm-hero__caption .fadeInRight-1').stop().delay(500).animate({ opacity: 1, left: '0' }, { duration: 800, easing: 'easeOutCubic' });
		$hero.find('.active .psm-hero__caption .fadeInRight-2').stop().delay(700).animate({ opacity: 1, left: '0' }, { duration: 800, easing: 'easeOutCubic' });
		$hero.find('.active .psm-hero__caption .fadeInRight-3').stop().delay(1000).animate({ opacity: 1, left: '0' }, { duration: 800, easing: 'easeOutCubic' });
    },

    fadeInLeftReset: function() {
		if (!dragging) {
            $hero.find('.psm-hero__caption .fadeInLeft-1, .psm-hero__caption .fadeInLeft-2, .psm-hero__caption .fadeInLeft-3').stop().delay(800).animate({ opacity: 0, left: '15px' }, { duration: 400, easing: 'easeInCubic' });
		} else {
            $hero.find('.psm-hero__caption .fadeInLeft-1, .psm-hero__caption .fadeInLeft-2, .psm-hero__caption .fadeInLeft-3').css({ opacity: 0, left: '15px' });
		}
	},
	
	fadeInRightReset: function() {
		if (!dragging) {
            $hero.find('.psm-hero__caption .fadeInRight-1, .psm-hero__caption .fadeInRight-2, .psm-hero__caption .fadeInRight-3').stop().delay(800).animate({ opacity: 0, left: '-15px' }, { duration: 400, easing: 'easeInCubic' });
		} else {
            $hero.find('.psm-hero__caption .fadeInRight-1, .psm-hero__caption .fadeInRight-2, .psm-hero__caption .fadeInRight-3').css({ opacity: 0, left: '-15px' });
		}
	}
};

var psmValidateFormSearch = {
    load: function () {
        $form_search.validate({
            rules: {
                txt_buscar: 'required'
            },
            messages: {
                txt_buscar: 'Por favor ingrese su contenido a buscar'
            },
            errorElement: 'em',
            errorPlacement: function (error, element) {
                $(element).parents('.psm-search__input').addClass('has-error');
            },
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.psm-search__input').addClass("has-error").removeClass('has-success');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.psm-search__input').addClass("has-success").removeClass('has-error');
            },
            submitHandler: function () {
                $btn_search.addClass('lq-btn--load');

                //AJAX
                $.ajax({
                    type: 'post',
                    url: ADMIN_URL,
                    data: {
                        action: '',
                        dataString: $form_search.serialize()
                    },
                    success: function (response) {
                    },
                    error: function (err) {
                    }
                });
                //END AJAX
            }
        });
    }
};

var psmSutmitFormSearch = {
    load: function () {
        $btn_search.on('click', function () {
            psmValidateFormSearch.load();
        });
    }
};

var psmSliderBrand = {
    load: function () {
        $brand_carousel.owlCarousel({
            autoPlay: false,
            stopOnHover: true,
            navigation: false,
            pagination: true,
            singleItem: true,
            addClassActive: true,
            transitionStyle: 'fade'
        })
    }
};

var psmInputToggle = {
    load: function () {
        $input_toggle.bootstrapToggle({
            on: '',
            off: ''
        });

        $input_toggle.change(function() {
            if($(this).prop('checked')) {
                $input_checkbox.prop('checked', true);
            } else {
                $input_checkbox.prop('checked', false);
            }
        });
    }
};

var psmformResetSubscribe = {
    load: function () {

        $form_subscribe_input.each(function () {
            var input = $(this);
            input.val('');
        });

        $input_toggle.bootstrapToggle('off');
        $input_checkbox.prop('checked', false);
        $form_subscribe_input_checkbox.prop('checked', false);
        
    }
};

var psmValidateFormSubscribe = {
    load: function () {
        $form_subscribe.validate({
            rules: {
                name: 'required',
                email: {
                    required: true,
                    email: true
                },
                telephone: {
                    required: true,
                    number: true,
                    minlength: 9
                }
            },
            messages: {
                name: 'Por favor ingrese su nombre',
                email: {
                    required: 'Necesitamos su dirección de correo electrónico para contactarlo',
                    email: 'Su dirección de correo electrónico debe estar en el formato de nombre@dominio.com'
                },
                telephone: {
                    required: 'Por favor ingrese su telefono',
                    number: 'No es un telefono valido',
                    minlength: 'No es un telefono valido'
                }
            },
            errorElement: 'em',
            errorPlacement: function (error, element) {
                if ($(element).prop('type') === 'checkbox') {
                    $(element).parents('.checkbox').addClass('has-error');
                }

                $(element).parents('.form-group').addClass('has-error');
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).prop('type') === 'checkbox') {
                    $(element).parents('.checkbox').addClass('has-error').removeClass('has-success');
                }

                $(element).parents('.form-group').addClass('has-error').removeClass('has-success');
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).prop('type') === 'checkbox') {
                    $(element).parents('.checkbox').addClass("has-success").removeClass('has-error');
                }

                $(element).parents('.form-group').addClass('has-success').removeClass('has-error');
            },
            submitHandler: function () {
                //AJAX
                $.ajax({
                    type: 'post',
                    url: ADMIN_URL,
                    data: {
                        action: '',
                        dataString: $form_subscribe.serialize()
                    },
                    success: function (response) {
                        psmModal.load('psm-subscribe__thank');
                    },
                    error: function (err) {
                        psmModal.load('psm-subscribe__error');
                    }
                });
                //END AJAX
            }
        });
    }
};

var psmSutmitFormSubscribe = {
    load: function () {
        $btn_subscribe.on('click', function () {
            psmValidateFormSubscribe.load();
        });
    }
};

var psmShowModal = {
    load: function (modal) {
        $('#' + modal).modal({
            show: true,
            keyboard: true
        });
    }
};

var psmModal = {
    load: function (modal) {

        if (modal) {

            if (modal === 'psm-subscribe__thank') {
                psmShowModal.load(modal);
                $('#' + modal).on('hidden.bs.modal', function () {
                    psmformResetSubscribe.load();
                });

            } else {
                psmShowModal.load(modal);
            }
        }

        $btn_modal.on('click', function () {
            var modal = $(this).data('modal');
            psmShowModal.load(modal);
        });
    }
};

var init = {
    funciones: function () {
        cache.load();
        
        if ($section_hero.length !== 0) {
            psmSliderHero.load();
        }

        if ($search.length !== 0) {
            psmSutmitFormSearch.load();
        }

        if ($brand_details.length !== 0) {
            psmSliderBrand.load();
        }

        if ($input_toggle !== 0) {
            psmInputToggle.load();
        }

        if ($form_subscribe.length !== 0) {
            psmSutmitFormSubscribe.load();
        }

        if ($btn_modal.length !== 0) {
            psmModal.load();
        }
    }
};

$(document).ready(init.funciones);